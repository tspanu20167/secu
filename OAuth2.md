# OAuth 2.0

C'est un protocole (framework) permettant à une application tierce d'obtenir des accès limités à un service HTTP, soit pour le compte du propriétaire soit en autorisant une l'application à l'utiliser pour son propre compte.

### Différents rôles :

 - Ressource - *ex : google agenda*
 - Propriétaire de la ressource *
 - Client souhaitant utiliser la ressource -  *application web ou standard*
 - Serveur d'autorisation - *Délivre les différents tickets*
 - Serveur stockant la ressource (ou service)

### Différents types d'organisation :
- Authorization code - *Web server authorization*

- Implicit - *Front end authorization (web browser)*

- Ressource owner credential - *Ex : android qui va faire la demande d'autorisation*

- Client credential - *Le client gère lui même la demande de ressource*





<!--stackedit_data:
eyJoaXN0b3J5IjpbLTc5NTQ2MTMzMCwxMjQyNDI1NjgyLDg0MD
A3NjA3NF19
-->