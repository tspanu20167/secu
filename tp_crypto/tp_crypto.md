#  TP Crypto


## Partie 1 :
### 1.2 OpenSSL 
OpenSSL est une boîte à outils de chiffrement comportant deux bibliothèques, libcrypto et libssl, fournissant respectivement une implémentation des algorithmes cryptographiques et du protocole de communication SSL/TLS, ainsi qu'une interface en ligne de commande, OpenSSL. 
Avec la commande OpenSSL, on peut faire : 

 - La création de clés RSA
 - La création de certificats en norme X.509
 - Le calcul d'empreintes (SHA, MD5, …)
 - Le chiffrement et déchiffrement (DES, AES, RC4, …)
 - La réalisation de tests de clients et serveurs SSL/TLS
 - La signature et le chiffrement de courriers (S/MIME)

Pour connaitre (sous Linux) les fonctionnalités de cette commande : man openssl La syntaxe générale de la commande OpenSSL est :

    openssl [options] 

### Exercice 1.3 :

Pour chiffrer le fichier MonF1 avec le système aes en mode CBC, avec une clé générée par mot de passe, le chiffré étant stocké dans le fichier MohF1.chiffre, on utilise la commande : 

    openssl enc -e –aes-128-cbc -in MonF1 -out MonF1.chiffre 

Si on ne met pas –e, la commande est aussi juste pour le chiffrement.

Commande de chiffrement :

    openssl enc -e -aes-128-cbc -in MonF1 -out MonF1.chiffre
    
Commande de déchiffrement :

    openssl enc -d -aes-128-cbc -in MonF1.chiffre -out MonF1.dechiffre

On compare la différence de taille de fichier :

    drwxr-xr-x 2 root    root    4096 oct.   2 06:26 ./ drwxr-xr-x 6 thierry thierry 4096 oct.   2 06:22 ../
    -rw-r--r-- 1 root    root      28 oct.   2 06:23 MonF1
    -rw-r--r-- 1 root    root      48 oct.   2 06:24 MonF1.chiffre
    -rw-r--r-- 1 root    root      28 oct.   2 06:25 MonF1.dechiffre

La taille du fichier clair est différent de celui du fichier crypté.

### Exercice 1.4 :
Clé chiffrée base 64 : c2VjcmV0
Clé déchiffrée : secret
Message clair : Passer au point suivant chiffrement avec clé

## Exercice 2 :

### Exercice 2.1 :

### Exercice 2.2 :

### Exercice 2.3 :

Commande création de clés RSA  :

    openssl genrsa -out Clersa.pem 1024

Commande visualisation clés RSA :

    openssl rsa -in -text -noout

Sur deux clés différentes, le e est toujours égal à 65537, la raison est que openssl fourni un chiffre qui soit un bon compromis entre un temps de calcul raisonnable et une sécurité satisfaisante.

### Exercice 2.4 :

Clé RSA Clersa.pem avant chiffrement :

     -----BEGIN RSA PRIVATE KEY-----
    MIICXQIBAAKBgQDC+7i7GTeD12Bk4JVI1RCB9W7hvbBnqPyU3x5qqL55xZlIKcmN
    n2KJxRdQP+bUQPD/kzsjbh/eDyg08FXjnNn0ldgM4czz+IXQlcNo6m8TJW0rZstP
    s3T74hh99iVvdIiFO3USbTgrv0CoJ9dK6Rh48/w96I5C4gvw1C4oXq54YQIDAQAB
    AoGBAL5DwfSY28pETaTCXgovFyeppqSTZGMSzD49JsIvnYbYIgX/+NTAtff4IhLF
    +GogYMsUEX42RNR8d+CIsVclRKwpvueDNf+rpD/elACHT1GapiDAQJ6eczDz4H0X
    2GCdzhuoSv1vkVN2GdZ3BKJwmoA0oEGwQRX6zQOMCkqoLCjRAkEA42zBQ+237sXg
    1Z/TXFTJUzABTN17xfoEbOZ5R6Ejpe8WiM+ckbUqPUhzqiuIdI2okiz+IWgHAsLO
    4DovZcEpnQJBANt7d3tX8YO0FNbx6eseVv3QOC206KWxsuzQLfO0MOG2CVWqIAJv
    OwrC1oHtAPM8SFp5XsBoulRY6lJ3TWQiQJUCQHBwyrTdCT1iVCEKXiOeP9ODPoFU
    V+ewDxQQdxH1F3zo0oldrBlWKsYV1iniUBrgFuH+oL6/ggfZbRLrcZergvkCQFgE
    mZPjXr18eGcCuNZLYoOC/ySFSk84hhKmIed7uaaqw++QvCl9xeW44SFVlG33HOT7
    i14KIb81sTLFGUow2kkCQQCd4XhL2fAxIy57Kgx/+opWJixt79gr4iQ/4OdyeTe/
    LR4gO7GT4FCSQjIFj4KOsZHxmRbwJpx5OuistWxd+izJ
    -----END RSA PRIVATE KEY-----

Commande de chiffrement Des :

    openssl rsa -in Clersa.pem -des3 -out Clersa.pem

Clé RSA après chiffrement :

     -----BEGIN RSA PRIVATE KEY-----
    Proc-Type: 4,ENCRYPTED
    DEK-Info: DES-EDE3-CBC,0DBD796BBFF5ECF2
    
    3DxpR5SN+lZlV0XhxcZ5kt+BRpEIdFzcSygZroOHnujFUdUnRmaDS8rQ+JBHB2z/
    DkJ7WpsDsjrwddv3piShcR3aBVqeWzwRuQepxdkWemgmKVDB8tHO2Pwcr+XBLPNF
    n12zm7Fu6w/F4GETv7nno/PhHokLKYSx0KxN8clO9Gczc5Ydm2uFkN87Ux5nx8fm
    AHL1BPgNIPOWc6o9O/g5yBMinpSYTMIn9+tpRwnGGmMw7borwGjrN0X5pfKsxcGs
    4jX15vRP6ofRtR8EYi9cHFCpaWYHvId25Mz7p8GEOB8izFXSJZbim/USMGXJ95+c
    +CK37TW8bksYTuJQDDkuo8nhM0mrFENhUfBxN2vYJgboTAyidrAjlZEdy6BUJ5OB
    9XBgfU9nH6bNMzQq7ZIP8h4Up6VqwrAhH8nX6pefUffmoQH/JgECcnUciARG78j8
    cWNOHYTDuzrIOTXbr22blbjJmehH13NOZf12O/wuKdei+jeTWK2pMCCt5Gyxr6+r
    7xs5R0CPquThOkGLpkFKg0e62B8bx48u0MgcAh5yHccNFv4lRj6BMzvAX94eFQyo
    LjTFB7T6bhzv6uoAqrNKZTwfUcqotKxpJJ0X18aWVcUsE3o+UVvT7rF1p6hVX/wQ
    csrSLla63WvmOi+t59dyvSveiKGNU3kthtfkVHA8UTFzKN79tXLPm+nlYTE6WThK
    C4yUadtlTuKzMYjaTv3nRKfxDlWa/9uidYIKF1yil65t0zJVAXpn0n6cyu5hrufd
    KRGobKl0f3bfANBK1gyeaoLfGA7MlIk5iDW4lZGorVpfCY6kLPZkqg==
    -----END RSA PRIVATE KEY-----

### Exercice 2.5 :

Commande création d'une clé de session 256 bits :

    openssl rand -out CleSession 256

Commande visualisation de la clé de session :

    hexdump CleSession

    0000000 958a 039d d236 0800 2cb0 984d e9b6 e687
    0000010 3322 a43d 9ab2 e2a1 8c3d 4494 cebe e135
    0000020 6bc1 89a8 3428 0fb2 8cff 8c1e 91c1 52dc
    0000030 a3d5 4776 50c7 2251 a8d0 3ba8 1caa eef1
    0000040 2ab4 2d10 03f2 9f8a b07d fdb7 2626 94e6
    0000050 19df 2f61 a5cf e4c7 5515 05b6 2352 fb8b
    0000060 8da1 5c5b 6f41 cd29 eb6c c658 01c3 7c0e
    0000070 cf9d f70c 41a6 136a 4dec b78f 51c2 2c34
    0000080 85b2 6877 3f3c 85fd 56cf d59e 26bd 8a9f
    0000090 ba63 d02a aebf 25d1 b07f 621f 925f 01df
    00000a0 c805 8670 0ecf 50eb c04d 71a9 73c7 ac48
    00000b0 8aa5 781f 5700 b5d1 34b8 ca63 2a33 8f75
    00000c0 356a cb48 c7b1 9698 662c e3fa bab0 5d9b
    00000d0 8935 13f5 a3b7 9eef 45c8 c6fd 88ed e006
    00000e0 e29e aa23 7a00 21c4 d3ce 9c6a a7e7 d68d
    00000f0 3be9 5d86 3ab0 4d49 1c76 9d45 5aa3 66a5
    0000100

Chiffrement de la clé de session avec la clé privé : 

    openssl rsautl -encrypt -in MonF1 -inkey Clersa2.pem -out MonF1.chiffre
(J'ai utilisé le fichier MonF1 car la clé de session était trop lourde pour la clé de chiffrement).

Déchiffrement  :
```
openssl rsautl -decrypt -in MonF1 -inkey Clersa2.pem -out MonF1.chiffre
```
### Exercice 2.6 :
Exportation de la partie publique de la clé :

    openssl rsa -in Clersa2.pem -pubout -out Clersa2Publique.pem

 Visualisation de la clé publique :

    openssl rsa -in Clersa2Publique.pem -pubin -text -noout
        RSA Public-Key: (1024 bit)
    Modulus:
        00:e6:c4:0e:a2:ae:59:2f:1c:3a:eb:30:db:9b:b5:
        f9:0f:d2:f4:f2:f2:b0:56:3d:85:42:13:11:ab:77:
        d4:fa:49:b3:dd:83:aa:07:a0:28:3c:b4:67:30:65:
        1f:c5:7b:aa:18:3f:09:5c:0e:bf:f2:2f:e8:e9:6a:
        68:20:93:45:5d:f6:eb:67:76:3b:4d:e0:18:ae:c4:
        41:19:52:4b:8b:4e:3e:7d:0f:c0:6c:01:ad:88:16:
        7a:f8:67:c1:97:32:75:a0:79:72:fd:b9:4b:fa:87:
        05:b5:b6:c8:ec:24:67:f3:82:29:33:12:53:2c:10:
        b7:6e:2d:db:df:07:54:f2:1f
    Exponent: 65537 (0x10001)

### Exercice 2.7

Création d'un fichier d'empreinte  :

    openssl dgst -SHA256 -out fprint MonF2
    
    SHA256(MonF2)= 29fd840bc271290b908e17268d5c9d05d727aeb474e460a959082b0af08b3408

Après modification d'une seule lettre dans le fichier :

    SHA256(MonF2)= 24e164e3e0016249d4caf06c0635b85ff58b479a551f6cd70f9d49af821819b3

Signature du fichier d'empreinte :

    openssl rsautl -sign -in fprint -inkey Clersa2.pem -out sign
> (ù^Nv«§<83>V<8d>6^U¨,ëe6<9b>EsC¬<8a><99>^X<97>Z¶VÔ2^Z§¤ÜTI<84>¨<85><92>þV¶<80>Øê<8b>È!Ðåi¶<98>ÇrXà>¹iæTO+g_I5Ä*<84>¤^WÁ<98>¿¨©TÖ^BËÐ7<89>­^_5TfÍ^B8<8d>w<91>5%9^^<8d><99>Í¯þº¤ç!)^]Þ<94>-£Hl^S
> xl¦µ¿¸!

Pour comparer vérifier un signature on calcul l'empreinte du fichier clair :

    openssl dgst -SHA256 -out fpverif clair
Ensuite, on vérifie la signature grâce à l'empreinte calculée et la clé publique fournie :

    openssl dgst -signature signature -verify maClePub.pem fpverif

On obtient le résultat  : Verification Failure
La signature fournie n'est donc pas la signature du fichier clair.

### Exercice 2.8 :

Création de la paire de clé rsa :

    openssl genrsa -out rsakey.pem 2048

Exportation de la clé publique  :

    openssl rsa -in rsakey.pem -pubout -out pkey.pem

### Exercice 2.9 :

Requête de création de certificat :

    openssl req -config openssl.cnf -new -key rsakey.pem -out pkeyreq.csr

Visualisation de la demande de certificat :

    openssl req -in maReq.csr -text
    Certificate Request:
        Data:
            Version: 1 (0x0)
            Subject: C = FR, ST = Rh\C3\83\C2\B4ne (69), L = Villeurbanne, O = Ecole CPE Lyon, OU = IRC, CN = MLM, emailAddress = mlm@cpe.fr
            Subject Public Key Info:
                Public Key Algorithm: rsaEncryption
                    RSA Public-Key: (2048 bit)
                    Modulus:
                        00:e0:e8:38:fc:ab:90:58:3b:11:b6:c0:4d:23:30:
                        f4:44:37:3b:56:a8:3f:6c:d5:79:55:a3:26:20:b0:
                        24:27:80:ee:0d:40:43:7c:cd:8b:42:68:8f:2c:21:
                        c7:f4:50:80:2e:3c:a6:46:71:c9:86:91:ce:e8:51:
                        ee:72:c4:6a:7f:ae:57:04:87:fb:e6:63:5b:7d:61:
                        21:65:72:21:c5:6c:60:84:bb:f5:df:43:4c:43:3b:
                        2e:ad:ba:34:4f:9b:24:81:8b:70:1f:c4:d5:89:46:
                        5a:20:61:95:39:8d:ba:d8:09:10:bb:27:27:da:ea:
                        15:91:c1:06:e1:c9:6b:0b:3f:c0:b4:6b:7b:24:be:
                        af:ec:38:da:c3:ad:2e:80:50:5c:bb:55:67:bd:50:
                        14:2c:9d:be:c4:f9:5e:e9:a4:cd:5a:e0:fe:83:5f:
                        4f:07:4a:38:dd:f7:a1:f4:d4:24:6b:40:4d:08:34:
                        d8:3e:0d:3e:93:54:2b:ed:02:6a:e0:bc:93:b9:3c:
                        01:de:43:48:20:a3:ac:0f:47:bf:58:32:fb:30:51:
                        f5:11:5c:96:ac:f1:bf:02:b4:c9:aa:d5:ee:2c:5a:
                        df:dc:c9:d7:2b:1c:55:47:71:6e:00:c2:3d:bd:60:
                        bc:aa:49:33:fb:b7:fc:31:b7:ee:8e:0a:0b:1b:18:
                        34:3d
                    Exponent: 65537 (0x10001)
            Attributes:
                a0:00
        Signature Algorithm: sha256WithRSAEncryption
             4a:04:f4:22:13:39:34:aa:43:39:42:e8:3a:4c:74:87:bb:eb:
             d5:71:59:21:79:b5:dc:33:bf:e2:98:60:0c:73:3d:7b:8b:78:
             9c:e3:18:f8:12:84:41:76:bf:fc:44:f1:0c:39:66:18:e4:27:
             2a:28:86:13:c9:6a:0d:09:29:ef:be:d6:e1:45:85:91:0e:0b:
             5b:b1:51:45:cd:d7:18:ef:8d:52:ab:89:99:9f:9b:26:14:6b:
             c9:d6:35:31:1f:40:af:57:72:43:c1:1f:72:53:06:2f:bf:e3:
             77:b4:94:5e:39:49:46:f6:52:f8:fe:82:9d:77:c7:0d:97:d2:
             1a:4b:92:fa:bd:62:cc:ca:e3:29:9a:66:bd:4c:f9:0f:fb:87:
             63:db:e4:18:d3:76:22:c8:71:02:fd:a0:9b:80:22:39:7b:c1:
             28:c8:f5:a2:30:70:bd:dc:7e:b5:e3:8b:07:0e:2f:f5:cb:da:
             02:8b:9a:e4:81:25:ee:80:a1:13:18:80:4e:04:95:85:ff:29:
             8f:01:df:e2:ac:8f:1c:95:65:59:07:19:28:af:fe:76:5a:fe:
             5c:fa:ce:a0:c1:81:80:ea:50:3c:26:ed:3c:24:b2:26:e5:79:
             ce:ed:ca:b7:14:d8:76:66:02:0d:65:21:eb:95:27:18:a8:26:
             f5:10:96:7c
    -----BEGIN CERTIFICATE REQUEST-----
    MIIC0jCCAboCAQAwgYwxCzAJBgNVBAYTAkZSMRYwFAYDVQQIDA1SaMODwrRuZSAo
    NjkpMRUwEwYDVQQHDAxWaWxsZXVyYmFubmUxFzAVBgNVBAoMDkVjb2xlIENQRSBM
    eW9uMQwwCgYDVQQLDANJUkMxDDAKBgNVBAMMA01MTTEZMBcGCSqGSIb3DQEJARYK
    bWxtQGNwZS5mcjCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAODoOPyr
    kFg7EbbATSMw9EQ3O1aoP2zVeVWjJiCwJCeA7g1AQ3zNi0Jojywhx/RQgC48pkZx
    yYaRzuhR7nLEan+uVwSH++ZjW31hIWVyIcVsYIS79d9DTEM7Lq26NE+bJIGLcB/E
    1YlGWiBhlTmNutgJELsnJ9rqFZHBBuHJaws/wLRreyS+r+w42sOtLoBQXLtVZ71Q
    FCydvsT5XumkzVrg/oNfTwdKON33ofTUJGtATQg02D4NPpNUK+0CauC8k7k8Ad5D
    SCCjrA9Hv1gy+zBR9RFclqzxvwK0yarV7ixa39zJ1yscVUdxbgDCPb1gvKpJM/u3
    /DG37o4KCxsYND0CAwEAAaAAMA0GCSqGSIb3DQEBCwUAA4IBAQBKBPQiEzk0qkM5
    Qug6THSHu+vVcVkhebXcM7/imGAMcz17i3ic4xj4EoRBdr/8RPEMOWYY5CcqKIYT
    yWoNCSnvvtbhRYWRDgtbsVFFzdcY741Sq4mZn5smFGvJ1jUxH0CvV3JDwR9yUwYv
    v+N3tJReOUlG9lL4/oKdd8cNl9IaS5L6vWLMyuMpmma9TPkP+4dj2+QY03YiyHEC
    /aCbgCI5e8EoyPWiMHC93H6144sHDi/1y9oCi5rkgSXugKETGIBOBJWF/ymPAd/i
    rI8clWVZBxkor/52Wv5c+s6gwYGA6lA8Ju08JLIm5XnO7cq3FNh2ZgINZSHrlScY
    qCb1EJZ8
    -----END CERTIFICATE REQUEST-----

### Exercice 2.10 :

Création d'un certificat auto-signé :
