# Keberos

Protocole d'identification via via la distribution de tickets.
### Propriété :
- Client -Serveur
- Authentification et autorisation sécurisées sur le réseaux non-sécurisés
- Authentification mutuelle sujet <--> objet
- protège contre le eavedropping et replay attack
- Utilisation possible du chiffrage symétrique et asymétrique.

Au début utiliser pour l'authentification entreprise <--> Internet il est aujourd'hui utilisé au sein de systèmes d'exploitation comme Windows.

#### Rappel : 
Les attaques de type replay consiste à intercepté un message (même crypté et à le rejouer plusieurs fois. **Kerberos** est insensible à ce genre d'attaque.

### Fonctionnement :
**AS** : *Authentication Server* - Vérifie que le client est bien connu.

**KDC** : *Key Distribution Center* - Fournit les autorisations aux services demandés.

**TGS** :  *Ticket Granting Service* - Reçoit les tickets d'autorisations et fournis les clés d'accès aux ressources demandées.

**SS** : *Service Server* - Serveur fournissant le service si les informations d'autorisations sont correctement transmises.

**TGT** : *Ticket Granting Ticket* - Ticket permettant d'accéder au TGS. Objet central.

### Faiblesses :
- le KDC est un point unique de défaillance et doit être constamment disponible. Si il est compromis, tout le système s'effondre car il fournit les tickets d'autorisation.

- Basé sur horodatage obligeant l'utilisation d'un serveur NTP.
- Pas de protocole d'administration standard.
- Seul l'information d'authentification est protégé, pas le trafic.

# SESAME :
### Objectifs :
- Technologie d'identification unique.
- Basé sur KERBEROS.
- Contrôle d'accès distribué.
- Utilisation du chiffrement asymétrique pour les transactions. Cela va diminuer grandement la complexité des échanges pour avoir une autorisation.

### Composition :
**AS** : *Authentication Server* - Authentifie l'utilisateur et fourni les jetons pour communiquer avec le PAS.

**PAS** : *Privilège Attribut Service* - Fournisseur de PAC permettant d'accéder aux ressources.

**PAC** : *Privilège Attribut Certificate* - Ticket permettant d'accéder aux ressources.

### Fonctionnement :

*Voir schéma*

### Faiblesses :
SESAME simplifie donc grandement l'authentification des utilisateurs grâce au chiffrement asymétrique. Cependant, ce chiffrement nécessite beaucoup plus de ressources de calcul pour être mis en œuvre. Ainsi, KERBEROS reste le plus utilisé dans les SE.




 



<!--stackedit_data:
eyJoaXN0b3J5IjpbLTcwOTEzMzgyMSwtMTgwNjgzMzAsLTE2Nz
Y4NDIxODAsLTgyODE4NzM1NSwtMTQ5ODkxOTY0MiwxMDIzMTA4
ODIzLC04MTQzMTM2ODgsLTMzMjQ1NTM2M119
-->